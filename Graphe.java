import java.util.*;
import java.io.*;

public class Graphe {
  private ArrayList<LinkedList<Integer>> incid;
  private ArrayList<LinkedList<Integer>> transp;

  public Graphe() {
    incid = new ArrayList<LinkedList<Integer>>();
    transp = new ArrayList<LinkedList<Integer>>();
  }

  public void addVertexIncid() {
    LinkedList<Integer> line = new LinkedList<Integer>();
    line.add(new Integer(0)); // dernier élèment représente marqué pour exploration
    incid.add(line);
  }

  public void addVertexIncid(LinkedList<Integer> line) {
    line.addLast(new Integer(0)); // dernier élèment représente marqué pour exploration
    incid.add(line);
  }

  public void addVertexTransp(LinkedList<Integer> line) {
    line.addLast(new Integer(0));
    transp.add(line);
  }

  /* Fonction utiliser pour ajouter un arc pour la création
    d'un graphe depuis un fichier */
  public void addDestForVertex(int source, Integer destination) {
    incid.get(source).removeLast();
    incid.get(source).addLast(destination);
    incid.get(source).addLast(new Integer(0));
  }

  public LinkedList<Integer> getVertex(int v, String mode) throws IllegalArgumentException{
    if (mode.equals("incidence"))
      return incid.get(v);
    else if (mode.equals("transpose"))
      return transp.get(v);
    else
      throw new IllegalArgumentException("Possible arguments : incidence, tranpose.");
  }

  public int getTotalVertices(){
      return incid.size();
  }

  public LinkedList<Integer> exploreVertexIncid(Integer v) {
    LinkedList<Integer> path = new LinkedList<Integer>();
    /*int v = getGUV();*/
    if (incid.get(v).getLast().equals(0)) { // Si pas exploré, le marquer comme tel
      incid.get(v).removeLast();
      incid.get(v).addLast(new Integer(1));
      for (int i=0; i<(incid.get(v).size()-1); i++) { //le dernier n'est pas un arc
        path.addAll(exploreVertexIncid(incid.get(v).get(i)));
      }
      path.add(new Integer(v)); // ordre suffixe

    }
    return path;
  }

  public LinkedList<Integer> exploreVertexTransp(Integer v) {
    LinkedList<Integer> path = new LinkedList<Integer>();
    if (transp.get(v).getLast().equals(0)) {
      transp.get(v).removeLast();
      transp.get(v).addLast(new Integer(1));
      for (int i=0; i<(transp.get(v).size()-1); i++) {
        path.addAll(exploreVertexTransp(transp.get(v).get(i)));
      }
      path.add(new Integer(v)); // ordre suffixe
    }
    return path;
  }

  public LinkedList<LinkedList<Integer>> exploreGraphIncid(){
    LinkedList<LinkedList<Integer>> listPaths = new LinkedList<LinkedList<Integer>>();
    while(hasUVertex())
      listPaths.addLast(exploreVertexIncid(getSUV(true)));
    return listPaths;
  }

  /** retourne la liste des sommets du premier parcours dans l'ordre inverse
      a fin de l'utiliser pour le deuxième parcours. **/
  public static LinkedList<Integer> getVerticesInOrder(LinkedList<LinkedList<Integer>> listPaths){
    LinkedList<Integer> listVertices = new LinkedList<Integer>();
    int listsLength = listPaths.size()-1;
    int elementLength = 0;
    while(listsLength>=0){
      elementLength = listPaths.get(listsLength).size()-1;
      while(elementLength>=0){
        listVertices.add(listPaths.get(listsLength).get(elementLength));
        elementLength--;
      }
      listsLength--;
    }
    return listVertices;
  }

  /** retourne une liste de composantes fortement connexes SCC. **/
  public LinkedList<LinkedList<Integer>> exploreGraphTransp(LinkedList<Integer> verticeInOrder)  {
    LinkedList<LinkedList<Integer>> listSCC = new LinkedList<LinkedList<Integer>>();
    for(int i=0; i<verticeInOrder.size(); i++){
      int v = verticeInOrder.get(i);
      if(transp.get(v).getLast().equals(0)) listSCC.addLast(exploreVertexTransp(v));
      else continue;
    }
    return listSCC;
  }

  /** Returns smaller undiscovered vertex or -1 if none
      Fonction utile pour le graph incid. **/
  public int getSUV(boolean verbose) {
    Integer smallerUndiscoveredVertex = new Integer(-1);

    if(verbose) System.out.println("> Searching SUV...");
    for (int i=0; i<incid.size(); i++) {
      if (incid.get(i).getLast().equals(0)) {
        smallerUndiscoveredVertex = new Integer(i);
        if(verbose)System.out.println("\tVertex "+i+" is the smallest undiscovered");
        break;
      }
    }
    return smallerUndiscoveredVertex;
  }

  public int getGUV(boolean verbose) {
    Integer greatestUndiscoveredVertex = new Integer(-1);

    if(verbose) System.out.println("Searching GUV...");
    for (int i=0; i<incid.size(); i++) {
      if (incid.get(i).getLast().equals(0)) {
        System.out.println("\tVertex "+i+" is undiscovered");
        if (i > greatestUndiscoveredVertex) {
          if(verbose) System.out.println("\tRegistering "+i+" as GUV (was "+greatestUndiscoveredVertex+")");
          greatestUndiscoveredVertex = new Integer(i);
        }
      }
    }
    return greatestUndiscoveredVertex;
  }

  /** Tests if still has undiscovered vertices
      Fonction utile pour le graph incid. **/
  public boolean hasUVertex() {
    if (getSUV(false) < 0)
      return false;
    else
      return true;
  }

  public boolean makeTransposed() throws ExistingTransposedException {
    if (!transp.isEmpty())
      throw new ExistingTransposedException("A transposed graph already exists. Clear it if a newer graph is needed.");

    LinkedList<Integer> currentVertex;

    int n = getTotalVertices();
    for (int i=0; i<n; i++) {
      LinkedList<Integer> vb = new LinkedList<Integer>();
      for (int j=0; j<n; j++) {
        currentVertex = incid.get(j);
        Iterator<Integer> iter2 = currentVertex.iterator();
        while (iter2.hasNext()) {
          if (iter2.next().equals(i) && iter2.hasNext()) // Vérifie que ce n'est pas le dernier car pas un sommet
            vb.addLast(new Integer(j));
        }
      }
      addVertexTransp(vb);
    }

    return true; // Vérification si réussi plus tard
  }

  public void clearTransposed() {
    transp.clear();
  }

  public String toString(String mode) throws IllegalArgumentException {
    ArrayList<LinkedList<Integer>> g;
    if (mode.equals("incidence"))
      g = incid;
    else if (mode.equals("transpose"))
      g = transp;
    else
      throw new IllegalArgumentException("Possible arguments : incidence, tranpose.");

    StringBuilder sbld = new StringBuilder();
    int n = getTotalVertices();
    LinkedList<Integer> currentVertex;

    sbld.append("[");
    for (int i=0; i<n; i++) {
      if (i == 0)
        sbld.append("[");
      else
        sbld.append(",\n[");
      currentVertex = g.get(i);
      Iterator<Integer> iter = currentVertex.iterator();
      while (iter.hasNext()) {
        Integer k = iter.next();
        if (iter.hasNext())
          sbld.append(k+",");
        else
          sbld.append(k);
      }
      sbld.append("]");
    }
    sbld.append("]");

    return sbld.toString();
  }

  ////////////
  //  2-SAT  //
  ////////////

  /* Prend en paramètre la liste des SCC calculer a la base de sommets indicé de 0 à 1
    et retourne la meme liste en changeant le nom (numéro) de chaque sommet par celui
    qui lui a été indiqué dans le fichier représentant la formule (entier positif et négatif) */
  public static LinkedList<LinkedList<Integer>> convertVerticesName(LinkedList<LinkedList<Integer>> scc, LinkedList<Integer> listOfRealVerticesN) {
    for (int i=0; i<scc.size(); i++) {
      for(int j=0; j<scc.get(i).size(); j++) {
        Integer realName = listOfRealVerticesN.get(scc.get(i).get(j));
        scc.get(i).set(j, realName);
      }
    }
    return scc;
  }

  public static boolean isSatisfactory(LinkedList<LinkedList<Integer>> scc) {
    for (int i=0; i<scc.size(); i++) {
      for(int j=0; j<scc.get(i).size(); j++) {
        for(int k=0; k<scc.get(i).size(); k++){
          if(k==j)
            continue; // ne pas comparer un element avec lui meme
          if(Math.abs(scc.get(i).get(j)) == Math.abs(scc.get(i).get(k)))
            return false;
        }
      }
    }
    return true;
  }

  ////////////
  //  MAIN  //
  ////////////

  public static void main(String[] args) throws IOException  {
    Scanner sc = new Scanner(System.in);
    boolean exit = false;

    while (!exit) {
      System.out.println("> Choisissez un mode");
      System.out.println("\t (1) Constuire soi-même le graphe");
      System.out.println("\t (2) Lire une formule 2Sat à partir d'un fichier");
      System.out.print("<");
      String resp = sc.nextLine();
      Integer mode = Integer.parseInt(resp);

      if(mode==1) {
        Menu.modeA();
        exit = true;
      } else if(mode==2) {
        Menu.modeB();
        exit = true;
      } else {
        System.out.println("! Invalid selection");
      }
    }
  }
}
