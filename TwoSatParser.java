import java.util.*;
import java.io.*;

public class TwoSatParser {
  private static String filename;

  TwoSatParser(String filename) {
    this.filename = filename;
  };

  public Integer getTotalVerticesFromFile() throws IOException {
    BufferedReader br = null;
    String line;
    Integer maxVertex = 0;
    try
    {
       br = new BufferedReader(new FileReader(filename));
    }
    catch(FileNotFoundException exc)
    {
       System.out.println("! Error while opening file");
    }
    while ((line = br.readLine()) != null){
      String res[] = line.split(" ");
      Integer t1 = Integer.parseInt(res[0]);
      Integer t2 = Integer.parseInt(res[1]);
      if(Math.abs(t1)>maxVertex)
        maxVertex = Math.abs(t1);
      if(Math.abs(t2)>maxVertex)
        maxVertex = Math.abs(t2);
    }
    br.close();
    return (maxVertex*2); /* représente le nombre de sommets */
  }

  public int getTotalVerticesFromFile(BufferedReader br) throws IOException {
    String line;
    int maxVertex = 0;

    while ((line = br.readLine()) != null){
      String res[] = line.split(" ");
      int t1 = Integer.parseInt(res[0]);
      int t2 = Integer.parseInt(res[1]);
      if(Math.abs(t1)>maxVertex)
        maxVertex = Math.abs(t1);
      if(Math.abs(t2)>maxVertex)
        maxVertex = Math.abs(t2);
    }

    return (maxVertex*2); /* représente le nombre de sommets */
  }

  public Graphe makeGraphFromFile(LinkedList<Integer> listOfRealVerticesN) throws IOException {
    BufferedReader br = null;
    String line;
    int totalVertices;
    FileInputStream iStream;
    Graphe g = new Graphe();

    try {
       iStream = new FileInputStream(filename);
    } catch(FileNotFoundException exc) {
       System.out.println("> Error while opening file");
       return null;
    }

    //Première passe sur le fichier pour obtenir le nombre de sommets
     br = new BufferedReader(new InputStreamReader(iStream));
     totalVertices = getTotalVerticesFromFile(br);
    //On remet la position du curseur dans le fichier à zéro
    iStream.getChannel().position(0);
    br = new BufferedReader(new InputStreamReader(iStream));

    System.out.println("> Initialization");
    for(int i=0; i<totalVertices; i++){
      g.addVertexIncid(); // Pas de paramètres => liste vide
      listOfRealVerticesN.addLast(new Integer(0));
      System.out.println("\tAdding vertex : "+i+"> "+g.getVertex(i, "incidence"));
    }

    System.out.println("> Making graph from file with positif vertices names");
    while ((line = br.readLine()) != null){
      LinkedList<Integer> l = new LinkedList<Integer>();
      String res[] = line.split(" ");
      Integer t1 = Integer.parseInt(res[0]);
      Integer t2 = Integer.parseInt(res[1]);
      Integer a, b, aBis, bBis;
      if(t1>0) a = t1-1;
      else a = Math.abs(t1) + ((totalVertices / 2) - 1);
      if(t2>0) b = t2-1;
      else b = Math.abs(t2) + ((totalVertices / 2) - 1);
      System.out.println("\tAdding 2 edges from the clause ("+a+" V "+b+")");
      if(t1>0) aBis = a + (totalVertices / 2);
      else aBis = a - (totalVertices / 2);
      if(t2>0) bBis = b + (totalVertices / 2);
      else bBis = b - (totalVertices / 2);
      /* Ajouter un commentaire */
      listOfRealVerticesN.set(a, t1);
      listOfRealVerticesN.set(b, t2);
      listOfRealVerticesN.set(aBis, -(t1));
      listOfRealVerticesN.set(bBis, -(t2));
      /* **** */
      g.addDestForVertex(aBis, b);
      System.out.println("\t\tUpdating vertex : "+aBis+"> "+g.getVertex(aBis, "incidence"));
      g.addDestForVertex(bBis, a);
      System.out.println("\t\tUpdating vertex : "+bBis+"> "+g.getVertex(bBis, "incidence"));
    }

    try {
      br.close();
    } catch (IOException exc) {
      System.out.println("! Closing buffer failed");
    }

    return g;
  }




}
