public class ExistingTransposedException extends RuntimeException {
  public ExistingTransposedException() {
    super();
  }

  public ExistingTransposedException(String message) {
    super(message);
  }

  public ExistingTransposedException(String message, Throwable cause) {
    super(message, cause);
  }

  public ExistingTransposedException(Throwable cause) {
    super(cause);
  }
}
