Algorithmique Avancée : Devoir 2
================================

Recherche des composantes fortement connexes d’un graphe
--------------------------------------------------------
**Dépôt Bitbucket :** [Lien](https://bitbucket.org/walensis/comfoconn.git)

## Team
Soufiane BENNANI, Hamza EL MEKNASSI, Majda BOUGHANMI.

## Synopsis
Nous allons coder un algorithme de recherche des composantes fortement connexes
d’un graphe. Au passage, nous résolverons aussi le problème du tri topologique
d’un graphe acyclique. Comme application, nous donnerons un algorithme décidant
si une formule propositionnelle de la forme 2-Sat possède une assignation qui
la satisfait.

## Usage
1. Exécuter le programme `java Graphe`
2. Choisissez un mode
         (1) Constuire soi-même le graphe et récupérer ses composantes fortement connexes
         (2) Lire une formule 2SAT à partir d'un fichier, constuire son graphe associé puis récupérer ses CFC

## Exécution
### La classe Menu  
Cette classe contient deux méthodes `modeA()` et `modeB()` correspondant respectivement :
- au mode d'entrée manuel où l'on indique à chaque invite vers quels sommets se dirige le sommet courant (ce mode ne supporte pas les sommets négatifs)
- le mode d'entrée fichier suivant lequel le programme nous invite à saisir le chemin du fichier contenant la formule 2-SAT.

### La classe Graphe
Elle comporte la majorité des fonctionnalités du programme. Elle possède les deux membres *incid* et *transp* qui vont stocker la matrice d'incidence et la matrice du graphe transposé correspondant. La structure se constitue d'une *ArrayList* contenant dans ses cases des *LinkedList* qui représentent les sommets implémentés par l'objet *Integer*, vers lesquels pointe le sommet courant qui n'est autre que l'indice de la case.

On observe aussi les accesseurs et mutateurs pour ces derniers `addVertexIncid()`, `addVertexTransp()`, `getVertex()` mais aussi une méthode `addDestForVertex()` pour modifier un sommet spécifique des matrices (i.e. ajouter un élément à une ligne de la matrice).

Ensuite se trouve les méthodes `exploreVertexIncid()` et `exploreVertexTransp()` qui s'occupent d'explorer une suite de sommets connectés récursivement des graphes et retournent une *LinkedList* des sommets parcourus. `exploreGraphIncid()` et `exploreGraphTransp()` sont des wrappers pour celles-ci, elles vont nous permettre de récupérer chacune des listes retournées et de les stocker dans une autre *LinkedList* englobante.

Après avoir éxecuté le premier parcours sur le graphe G, on se retrouve avec une liste de sommets dans l'ordre suffixe, sur laquelle on applique la méthode utilitaire `getVerticesInOrder()` qui l'ordonne par ordre inverse, afin que le parcours sur le graphe transposé de G explore les sommets dans le bon ordre.

S'en suit trois fonctions utilitaires `getSUV()`, `getGUV()` et `hasUVertex()` qui nous permettent de rechercher dans un graphe le sommet le plus petit non exploré (Smallest Undiscovered Vertex) ou le plus grand non exploré (Greatest Undiscovered Vertex) et pour la dernière de dire s'il existe encore un sommet non exploré en utilisant justement l'une des deux.

̀`makeTransposed()` doit être utilisée pour générer le transposé lorsque l'on a terminé de modifier la matrice *incid*. Pour générer une seconde fois le transposé on doit recourir à `clearTransposed()` avant d'exécuter la première méthode à nouveau.

La méthode ̀`toString()` est assez classique : elle permet de sérialiser (encoder?) grâce à un *StringBuilder* les matrices de la classe pour affichage ou stockage ultérieur. Nous l'utiliserons uniquement pour afficher les matrices ici.

On fait appel à la méthode `convertVerticesName` uniquement dans le *modeB* du programme (i.e. interprétation d'une formule 2-SAT) juste après avoir effectué l'algorithme de Kosaraju. Elle prend en paramètre la liste des composantes fortement connexes calculée sur une base de sommets indicés de 0 à n et retourne la même liste en réaffectant à chaque sommet son vrai nom (numéro?) comme il a été indiqué dans le fichier représentant la formule 2-Sat (entier positif/négatif)

Enfin, on retrouve la méthode `isSatisfactory` qui prend en paramétre une liste de composantes fortement connexes générer par le deuxieme parcours sur le graphe transposé de G, parcourt les elements de chaque composante et vérifie s'il existe un sommet et sa négation dans une même composante fortement connexes. si c'est le cas, la methode renvoie *false*, ce qui signifie que la formule 2-SAT est insatisfaisable.

### La classe TwoSatParser
Cette classe s'occupe d'interpréter un fichier contenant une formule propositionnelle 2-SAT en un graphe stockable dans notre structure. Pour ce faire elle possède un unique membre statique *filename* contenant le chemin vers ledit fichier et les fonctions suivantes :
- `getTotalVerticesFromFile()` : comme elle suggère retourne l'effectif des sommets que générerait la formule 2-SAT si l'on devait l'interpréter en un graphe en recherchant le sommet absolu le plus grand et en multipliant ce dernier par deux car par définition pour chaque sommet on retrouve son opposé dans le graphe,
- `makeGraphFromFile()` : en créant un graphe vide avec une phase d'initialisation, on insère grâce à ̀`addDestForVertex()` les destinations vers lesquelles s'orientent les sommets au fur et à mesure que la propriété "¬a→ b et ¬b → a" génère des arcs sur chaque ligne du fichier.

### La classe ExistingTransposedException
Elle hérite de la classe *Exception* et se produit lorsque l'on `makeTransposed()` sans avoir `clearTransposed()` au préalable alors qu'un transposé est déjà existant.
