import java.util.*;
import java.io.*;

public class Menu {

  public static void modeA() {
    Scanner sc = new Scanner(System.in);
    Graphe g = new Graphe();
    boolean loop = true;
    int v = 0;

    do {
       String str = new String();
       LinkedList<Integer> l = new LinkedList<Integer>();

       System.out.print("Set destinations for vertex "+v+" :\n<");
       str = sc.nextLine();
       if (str.equals("/")) {
         loop = false;
         continue;
       }
      for (int i=0; i<str.length(); i++) {
      int n = Character.getNumericValue(str.charAt(i));
      if (n<0 || str.charAt(i) == ' ') {
        //System.out.println("NaN : "+str.charAt(i));
        continue;
      }
      l.addLast(new Integer(n));
      }
      System.out.println("Adding vertex : "+ l);
      g.addVertexIncid(l);
      v++;
    } while (loop);
    System.out.println("> Final incidence matrix : \n"+g.toString("incidence"));
    g.makeTransposed();
    System.out.println("> Final transposed matrix : \n"+g.toString("transpose"));


    LinkedList<LinkedList<Integer>> listPaths = g.exploreGraphIncid();
    LinkedList<Integer> vio = Graphe.getVerticesInOrder(listPaths);
    LinkedList<LinkedList<Integer>> listSCC = g.exploreGraphTransp(vio);

    System.out.println("> Result of the first exploration of the graph : \n\t"+ listPaths);
    System.out.println("> Put the list in the right order for the second exploiration : \n\t"+ vio);
    System.out.println("> Strongly connected components resulting from the second exploration : \n\t"+ listSCC);
  }

  public static void modeB()  throws IOException {
    Scanner sc = new Scanner(System.in);
    LinkedList<Integer> listOfRealVerticesN = new LinkedList<Integer>();

    System.out.print("> Please enter the file name\n<");
    String file = sc.nextLine();
    System.out.println("> Parsing "+file+" ...");
    TwoSatParser parser = new TwoSatParser(file);
    Graphe g = parser.makeGraphFromFile(listOfRealVerticesN);
    if (g == null) {
      System.out.println("\tParsing from file failed");
      System.exit(1);
    }
    System.out.println("> List of real vertices name ");
    for(int i=0; i<listOfRealVerticesN.size(); i++){
      System.out.println("\tCode name:["+i+"] => Real name:["+listOfRealVerticesN.get(i)+"]");
    }
    System.out.println("> Final incidence matrix : \n"+g.toString("incidence"));
    g.makeTransposed();
    System.out.println("> Final transposed matrix : \n"+g.toString("transpose"));
    LinkedList<LinkedList<Integer>> listPaths = g.exploreGraphIncid();
    LinkedList<Integer> vio = Graphe.getVerticesInOrder(listPaths);
    LinkedList<LinkedList<Integer>> listSCC = g.exploreGraphTransp(vio);
    System.out.println("> Result of the first exploration of the graph : \n\t"+ listPaths);
    System.out.println("> Reordered list for the second exploration : \n\t"+ vio);
    System.out.println("> Strongly connected components resulting from the second exploration : \n\t"+ listSCC);
    LinkedList<LinkedList<Integer>> listSCCRN = Graphe.convertVerticesName(listSCC, listOfRealVerticesN );
    System.out.println("> Strongly connected components with real name : \n\t"+ listSCCRN);
    if(Graphe.isSatisfactory(listSCCRN))
      System.out.println("> Cette formule possède un assignement de valeurs aux variables qui la satisfait !");
    else
      System.out.println("> Cette formule ne possède un assignement de valeurs aux variables qui la satisfait !");
  }

}
